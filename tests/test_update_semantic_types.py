# import os
import json
import unittest

from common_primitives import dataset_to_dataframe, update_semantic_types
from d3m import container, utils
from d3m.metadata import base as metadata_base

import utils as test_utils


class UpdateSemanticTypesPrimitiveTestCase(unittest.TestCase):
    def test_basic(self):
        # load the iris dataset
        new_dataset = self._loadAndUpdate()

        # validate metadata
        semantic_types = new_dataset.metadata.query(('0', metadata_base.ALL_ELEMENTS, 1))['semantic_types']
        self.assertSetEqual(set(['http://schema.org/Text', 'https://metadata.datadrivendiscovery.org/types/Attribute']), set(semantic_types))

        semantic_types = new_dataset.metadata.query(('0', metadata_base.ALL_ELEMENTS, 3))['semantic_types']
        self.assertSetEqual(set(['http://schema.org/Integer', 'https://metadata.datadrivendiscovery.org/types/Attribute']), set(semantic_types))

    def test_with_convert(self):
        # convert to dataframe
        dataset = self._loadAndUpdate()
        ds2df_hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        ds2df_primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=ds2df_hyperparams_class.defaults())
        dataframe = ds2df_primitive.produce(inputs=dataset).value

        # sanity check the conversion
        semantic_types = dataframe.metadata.query((metadata_base.ALL_ELEMENTS, 1))['semantic_types']
        self.assertSetEqual(set(['http://schema.org/Text', 'https://metadata.datadrivendiscovery.org/types/Attribute']), set(semantic_types))

    def _loadAndUpdate(self):
        # load the iris dataset
        dataset = test_utils.load_iris_metadata()

        hyperparams = {
            'resource_id': '0',
            'add_columns': ('sepalLength', 'petalLength'),
            'add_types': ('http://schema.org/Text', 'http://schema.org/Integer'),
            'remove_columns': ('sepalLength', 'petalLength'),
            'remove_types': ('http://schema.org/Float', 'http://schema.org/Float')
        }
        update_types_primitive = update_semantic_types.UpdateSemanticTypesPrimitive(hyperparams=hyperparams)
        new_dataset = update_types_primitive.produce(inputs=dataset).value
        return new_dataset


if __name__ == '__main__':
    unittest.main()
